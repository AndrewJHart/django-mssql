Django ORM driver for MS SQL Server 2008
========================================

This repo is forked from `Django-mssql` - which provides a Django database backend for the ORM to work with Microsoft SQL Server.

This is a monkey patched fork that allows us to run the latest stable version of django (as of 1.9.3) in a linux environment whilst still allowing
us to use Django's ORM to query outdated or deprecated versions of Microsoft's SQL Server, for instance: this fork is tweaked to work with SQL
Server 2008 as a db backend via Django's ORM. 

**Note:** use at your own risk - this was last tested on django 1.9.3 & SQL Server 2008 ~6 years ago

---------

Features & Improvements in this fork:
=====================================

 - fork of a django 1.9 compatible version & works with django 1.9.3
 - Supports SQL Server 2008 
    - patched with older versions to work with SQL Server 2008 (mainly compiler.py)
 - nice adjustment that **fixes pagination** for django-mssql server 2008 by altering the select statement that uses row_number()
 - Used in conjunction with [django-sqlserver](https://github.com/denisenkom/django-sqlserver) repo to allow operation of django server on linux/osx.



Documentation is available at `django-mssql.readthedocs.org`_.

---------------

Requirements
------------

    * Python 2.7, 3.4
    * PyWin32_
    * SQL Server Management Studio or Microsoft Data Access Components (MDAC)

SQL Server Versions
-------------------

Supported Versions:
    * 2012

SQL 2008/2008r2 are support by django-mssql 1.6.x.

Django Version
--------------

	* Django 1.8

django-mssql 1.7 supports Django 1.7.
django-mssql 1.6 supports Django 1.6.
django-mssql 1.4 supports Django 1.4 and 1.5.


References
----------

    * Django-mssql on PyPi: http://pypi.python.org/pypi/django-mssql
    * DB-API 2.0 specification: http://www.python.org/dev/peps/pep-0249/


.. _`Django-mssql`: https://bitbucket.org/Manfre/django-mssql
.. _django-mssql.readthedocs.org: http://django-mssql.readthedocs.org/
.. _PyWin32: http://sourceforge.net/projects/pywin32/